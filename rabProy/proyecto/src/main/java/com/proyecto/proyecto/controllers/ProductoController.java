package com.proyecto.proyecto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.proyecto.proyecto.domain.Categoria;
import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.services.CategoriaService;
import com.proyecto.proyecto.services.ProductoService;

@Controller
@RequestMapping("/productos")
public class ProductoController {

    @Autowired
    public ProductoService productoService;

    @Autowired
    public CategoriaService categoriaService;

    @GetMapping({ "", "/" })
    public String showProducts(Model model) {
        model.addAttribute("listaProd", productoService.obtenerTodos());
        model.addAttribute("listaCategorias", categoriaService.mostrarListaCategorias());
        model.addAttribute("categoriaSeleccionada", new Categoria(0L, "Todas"));
        return "product/productsView";
    }

    @GetMapping("/nuevo")
    public String showNew(Model model) {
        model.addAttribute("productoForm", new Producto());
        model.addAttribute("listaCategorias", categoriaService.mostrarListaCategorias());
        return "product/newFormView";
    }

    @PostMapping("/nuevo/submit")
    public String showNewSubmit(Producto productoForm, Model model) {
        productoService.añadir(productoForm);
        return "redirect:/productos/";
    }

    @GetMapping("/editar/{id}")
    public String showEdit(@PathVariable Long id, Model model) {
        Producto productoForm;
        productoForm = productoService.obtenerPorId(id);
        model.addAttribute("productoForm", productoForm);
        model.addAttribute("listaCategorias", categoriaService.mostrarListaCategorias());
        return "product/editFormView";
    }

    @PostMapping("/editar/submit")
    public String showEditSubmit(Producto productoForm, Model model) {
        productoService.editar(productoForm);
        return "redirect:/productos/";
    }

    @GetMapping("/borrar/{id}")
    public String showDelete(@PathVariable Long id, Model model) {
        productoService.borrar(id);
        return "redirect:/productos/";
    }

    @GetMapping("/porCateg/{idCat}")
    public String showListInCategory(@PathVariable Long idCat, Model model) {
        model.addAttribute("listaProd", productoService.obtenerPorCategoria(idCat));
        model.addAttribute("listaCategorias", categoriaService.mostrarListaCategorias());
        model.addAttribute("categoriaSeleccionada", categoriaService.obtenerPorId(idCat));
        return "product/productsView";
    }
}
