package com.proyecto.proyecto.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.proyecto.proyecto.Dto.NuevoUsuario;
import com.proyecto.proyecto.domain.Rol;
import com.proyecto.proyecto.domain.Usuario;
import com.proyecto.proyecto.excepciones.UsuarioDuplicadoException;
import com.proyecto.proyecto.services.UsuarioService;

@Controller
@RequestMapping("/registro")
public class UsuarioController2 {
    
    @Autowired
    UsuarioService usuarioService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping("/nuevo")
    public String nuevoUsuario(Model model) {
        model.addAttribute("formUsuario", new NuevoUsuario());
        return "registro";
    }

    @PostMapping("/nuevo")
    public String nuevoUsuarioSubmit(NuevoUsuario nuevoUsuario, Model model) {
        Usuario usuario = modelMapper.map(nuevoUsuario, Usuario.class);
        usuario.setRol(Rol.USER);
        
        try {
            usuarioService.añadir(usuario);
        } catch (UsuarioDuplicadoException e) {
            model.addAttribute("formUsuario", nuevoUsuario);
            model.addAttribute("duplicado", true);
            return "registro";
        }
        
        return "redirect:/";
    }
}
