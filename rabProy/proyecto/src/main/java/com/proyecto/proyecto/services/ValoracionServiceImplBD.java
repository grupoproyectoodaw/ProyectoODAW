package com.proyecto.proyecto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.domain.Rol;
import com.proyecto.proyecto.domain.Usuario;
import com.proyecto.proyecto.domain.Valoracion;
import com.proyecto.proyecto.excepciones.ValoracionException;
import com.proyecto.proyecto.repositories.UsuarioRepository;
import com.proyecto.proyecto.repositories.ValoracionRepository;

@Service
public class ValoracionServiceImplBD implements ValoracionService {
    @Autowired
    ValoracionRepository valoracionRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    public Valoracion obtenerPorId(Long id) {
        return valoracionRepository.findById(id).orElse(null);
    }

    public Valoracion añadir(Valoracion valoracion) {
        return valoracionRepository.save(valoracion);
    }

    public List<Valoracion> mostrarListaValoracion() {
        return valoracionRepository.findAll();
    }

    public void borrar(Valoracion valoracion) {
        if (valoracion.getUsuario().getRol() == Rol.USER) {
            if (valoracion.getUsuario().getNombre() != obtenerUsuarioConectado().getNombre()) {
                throw new ValoracionException();
            }
        }
        valoracionRepository.delete(valoracion);
    }

    public List<Valoracion> obtenerPorUsuario(Usuario usuario) {
        return valoracionRepository.findByUsuario(usuario);
    }

    public List<Valoracion> obtenerPorProducto(Producto producto) {
        return valoracionRepository.findByProducto(producto);
    }

    public Valoracion obtenerPorUsuarioProducto(Usuario u, Producto p) {
        return valoracionRepository.findByUsuarioAndProducto(u, p);
    }

    public Usuario obtenerUsuarioConectado() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String nombreUsuarioConectado = authentication.getName();
            return usuarioRepository.findByNombre(nombreUsuarioConectado).get();
        }
        return null;
    }
}
