package com.proyecto.proyecto.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.proyecto.proyecto.domain.Carrito;
import com.proyecto.proyecto.domain.Cliente;
import com.proyecto.proyecto.domain.DetallePedido;
import com.proyecto.proyecto.domain.ItemCarrito;
import com.proyecto.proyecto.domain.Pedido;
import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.services.CarritoService;
import com.proyecto.proyecto.services.ClienteService;
import com.proyecto.proyecto.services.DetallePedidoService;
import com.proyecto.proyecto.services.PedidoService;
import com.proyecto.proyecto.services.ProductoService;

@Controller
public class CarritoController {
    @Autowired
    private Carrito carrito;

    @Autowired
    private CarritoService carritoService;

    @Autowired
    private PedidoService pedidoService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private DetallePedidoService detallePedidoService;

    @Autowired
    private ProductoService productoService;

    @GetMapping("/carrito")
    public String verCarrito(Model model) {
        List<ItemCarrito> items = carrito.getItems();

        double precioTotal = items.stream()
                .mapToDouble(item -> item.getProducto().getPrecio() * item.getCantidad())
                .sum();

        List<Cliente> clientes = clienteService.obtenerTodos();

        model.addAttribute("productos", items);
        model.addAttribute("precioTotal", precioTotal);
        model.addAttribute("clientes", clientes);

        return "/carrito/carritoView";
    }

    @PostMapping("/carrito/agregar/{id}")
    public String agregarProductoAlCarrito(@PathVariable Long id, @RequestParam("cantidad") int cantidad,
            RedirectAttributes redirectAttributes) {
        boolean agregado = carritoService.agregarProducto(id, cantidad);
        if (agregado) {
            redirectAttributes.addFlashAttribute("mensaje", "Producto agregado al carrito.");
        } else {
            redirectAttributes.addFlashAttribute("error", "No se pudo agregar el producto al carrito.");
        }
        return "redirect:/productos";
    }

    @PostMapping("/carrito/actualizar/{id}")
    public String actualizarCantidadProducto(@PathVariable Long id, @RequestParam("cantidad") int cantidad,
            RedirectAttributes redirectAttributes) {
        boolean actualizado = carritoService.actualizarCantidadProducto(id, cantidad);
        if (actualizado) {
            redirectAttributes.addFlashAttribute("mensaje", "Cantidad actualizada en el carrito.");
        } else {
            redirectAttributes.addFlashAttribute("error", "No se pudo actualizar la cantidad en el carrito.");
        }
        return "redirect:/carrito";
    }

    @PostMapping("/carrito/eliminar/{index}")
    public String eliminarProductoDelCarrito(@PathVariable int index, RedirectAttributes redirectAttributes) {
        carritoService.eliminarProducto(index);
        redirectAttributes.addFlashAttribute("mensaje", "Producto eliminado del carrito.");
        return "redirect:/carrito";
    }

    @PostMapping("/pagar")
public String procesarPago(@RequestParam("precioTotal") String precioTotalStr,
                           @RequestParam("clienteId") Long clienteId,
                           @RequestParam(value = "productos", required = false) List<Long> productoIds,
                           @RequestParam(value = "cantidades", required = false) List<Integer> cantidades,
                           RedirectAttributes redirectAttributes,
                           Model model) {

    try {
        Double precioTotal = Double.parseDouble(precioTotalStr);

        Cliente cliente = clienteService.obtenerPorId(clienteId);

        if (cliente == null) {
            throw new IllegalArgumentException("Cliente no encontrado con ID: " + clienteId);
        }

        Pedido nuevoPedido = new Pedido();
        nuevoPedido.setPrecio(precioTotal);
        nuevoPedido.setCliente(cliente);
        nuevoPedido.setFecha(LocalDate.now());

        pedidoService.añadir(nuevoPedido);

        if (productoIds != null && cantidades != null) {
            if (productoIds.size() != cantidades.size()) {
                throw new IllegalArgumentException("La lista de productos y cantidades debe tener la misma longitud.");
            }

            for (int i = 0; i < productoIds.size(); i++) {
                Long productoId = productoIds.get(i);
                Integer cantidad = cantidades.get(i);

                Producto producto = productoService.obtenerPorId(productoId);

                if (producto == null) {
                    throw new IllegalArgumentException("Producto no encontrado con ID: " + productoId);
                }

                DetallePedido detallePedido = new DetallePedido();
                detallePedido.setPedido(nuevoPedido);
                detallePedido.setProducto(producto);
                detallePedido.setCantidad(cantidad);

                detallePedidoService.añadir(detallePedido);
            }
        } else {
            throw new IllegalArgumentException("Los parámetros 'productos' y 'cantidades' son necesarios.");
        }

        redirectAttributes.addFlashAttribute("mensaje", "¡Pedido realizado con éxito!");

    } catch (NumberFormatException e) {
        redirectAttributes.addFlashAttribute("error", "Error al procesar el pago. Precio total inválido.");
    } catch (IllegalArgumentException e) {
        redirectAttributes.addFlashAttribute("error", e.getMessage());
    }

    return "redirect:/carrito";
}

}
