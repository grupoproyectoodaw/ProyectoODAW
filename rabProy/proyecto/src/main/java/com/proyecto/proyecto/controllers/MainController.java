package com.proyecto.proyecto.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;

import com.proyecto.proyecto.domain.FormInfo;
import com.proyecto.proyecto.services.ProductoServiceImplBD;

@RequestMapping("/")
@Controller
public class MainController {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    public ProductoServiceImplBD productoService;

    @GetMapping({ "/home", "/" })
    public String showHome(Model model) {
        return "main/homeView";
    }

    @GetMapping("/terminos")
    public String showTerminos(Model model) {
        return "main/terminos";
    }

    @GetMapping("/ayuda")
    public String showAyuda(Model model) {
        return "main/ayuda";
    }

    @GetMapping("/contact")
    public String showContact(Model model) {
        model.addAttribute("formInfo", new FormInfo());
        return "main/contactView";
    }

    @PostMapping("/contact/submit")
    public String showContactResult(FormInfo formInfo, Model model) {
        String nombre = formInfo.getNombre();
        String email = formInfo.getEmail();
        String lista = formInfo.getLista();
        String comentario = formInfo.getComentario();
        boolean check = formInfo.isCheck();
        model.addAttribute("nombre", nombre);
        model.addAttribute("email", email);
        model.addAttribute("lista", lista);
        model.addAttribute("comentario", comentario);
        model.addAttribute("check", check);

        model.addAttribute("alertMessage", "¡Mensaje enviado!");
        
        return "main/contactResultView";
    }

    @PostMapping("/enviar-mensaje")
    public String enviarMensaje(@RequestParam("mensaje") String mensaje, Model model) {
        String destinatario = "raul.agras04@gmail.com";
        String asunto = "Nuevo mensaje desde formulario de contacto";

        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom("tu-correo@trial-pq3enl601e7l2vwr.mlsender.net");
            mailMessage.setTo(destinatario);
            mailMessage.setSubject(asunto);
            mailMessage.setText(mensaje);
            mailSender.send(mailMessage);

            model.addAttribute("success", "Mensaje enviado con éxito");
        } catch (Exception e) {
            model.addAttribute("error", "Error al enviar el mensaje: " + e.getMessage());
        }
        return "main/ayuda";
    }
}
