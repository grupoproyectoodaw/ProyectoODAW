package com.proyecto.proyecto.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.domain.Usuario;
import com.proyecto.proyecto.domain.Valoracion;

@Repository
public interface ValoracionRepository extends JpaRepository<Valoracion, Long> {
    List<Valoracion> findByUsuario(Usuario usuario);

    List<Valoracion> findByProducto(Producto producto);

    Valoracion findByUsuarioAndProducto(Usuario usuario, Producto producto);
}
