package com.proyecto.proyecto.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.proyecto.domain.Carrito;
import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.repositories.ProductoRepository;

@Service
public class CarritoService {


    @Autowired
    private ProductoRepository productoRepository;

    @Autowired
    private ProductoService productoService;

    @Autowired
    private Carrito carrito;

    public boolean agregarProducto(Long id, int cantidad) {
        Producto producto = productoRepository.findById(id).orElse(null);
        if (producto != null && cantidad > 0 && cantidad <= producto.getCantidad()) {
            carrito.agregarProducto(producto, cantidad);
            producto.setCantidad(producto.getCantidad() - cantidad);
            productoService.actualizarProducto(producto);
            return true;
        }
        return false;
    }

    public boolean actualizarCantidadProducto(Long id, int cantidad) {
        return carrito.actualizarCantidadProducto(id, cantidad);
    }

    public void eliminarProducto(int index) {
        if (index >= 0 && index < carrito.getItems().size()) {
            carrito.getItems().remove(index);
        }
    }
    
    
    
    
}


