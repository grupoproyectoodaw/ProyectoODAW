package com.proyecto.proyecto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.proyecto.domain.Categoria;
import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.repositories.CategoriaRepository;
import com.proyecto.proyecto.repositories.ProductoRepository;

@Service
public class ProductoServiceImplBD implements ProductoService {
    @Autowired
    ProductoRepository productoRepository;

    @Autowired
    CategoriaRepository categoriaRepository;

    public Producto añadir(Producto producto) {
        return productoRepository.save(producto);
    }

    public void actualizarProducto(Producto producto) {
        productoRepository.save(producto);
    }

    public List<Producto> obtenerTodos() {
        return productoRepository.findAll();
    }

    public Producto obtenerPorId(Long id) {
        return productoRepository.findById(id).orElse(null);
    }

    public Producto editar(Producto producto) {
        return productoRepository.save(producto);
    }

    public void borrar(Long id) {
        productoRepository.deleteById(id);
    }

    public List<Producto> obtenerPorCategoria(Long idCategoria) {
        Categoria categoria = categoriaRepository.findById(idCategoria).orElse(null);
        if (categoria == null)
            return null;
        return productoRepository.findByCategoria(categoria);

    }
}
