package com.proyecto.proyecto;

import java.time.LocalDate;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.proyecto.proyecto.domain.Categoria;
import com.proyecto.proyecto.domain.Cliente;
import com.proyecto.proyecto.domain.DetallePedido;
import com.proyecto.proyecto.domain.Pedido;
import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.domain.Rol;
import com.proyecto.proyecto.domain.Usuario;
import com.proyecto.proyecto.services.CategoriaService;
import com.proyecto.proyecto.services.ClienteService;
import com.proyecto.proyecto.services.DetallePedidoService;
import com.proyecto.proyecto.services.PedidoService;
import com.proyecto.proyecto.services.ProductoService;
import com.proyecto.proyecto.services.UsuarioService;

@SpringBootApplication
public class ProyectoApplication {

        public static void main(String[] args) {
                SpringApplication.run(ProyectoApplication.class, args);
        }

        @Bean
        CommandLineRunner initData(ProductoService productoService, PedidoService pedidoService,
                        ClienteService clienteService, CategoriaService categoriaService,
                        UsuarioService usuarioService, DetallePedidoService detallePedidoService) {
                return args -> {
                        Categoria cat1 = categoriaService.añadir(
                                        new Categoria(0L, "Frutas"));
                        Categoria cat2 = categoriaService.añadir(
                                        new Categoria(0L, "Verduras"));
                        Categoria cat3 = categoriaService.añadir(
                                        new Categoria(0L, "Carnes"));
                        Categoria cat4 = categoriaService.añadir(
                                        new Categoria(0L, "Pescados"));
                        Categoria cat5 = categoriaService.añadir(
                                        new Categoria(0L, "Bebidas"));
                        Cliente cliente1 = clienteService.añadir(
                                        new Cliente(0L, "Juan", "Agras", "698159772", "juanagras@gmail.com",
                                                        "A Coruña"));
                        Cliente cliente2 = clienteService.añadir(
                                        new Cliente(0L, "María", "López", "654987321", "marialopez@gmail.com",
                                                        "Madrid"));

                        Cliente cliente3 = clienteService.añadir(
                                        new Cliente(0L, "Carlos", "González", "678234567", "carlosgonzalez@gmail.com",
                                                        "Barcelona"));

                        Cliente cliente4 = clienteService.añadir(
                                        new Cliente(0L, "Ana", "Martínez", "645781239", "anamartinez@gmail.com",
                                                        "Valencia"));

                        Cliente cliente5 = clienteService.añadir(
                                        new Cliente(0L, "Pedro", "Sánchez", "621349876", "pedrosanchez@gmail.com",
                                                        "Sevilla"));

                        Producto producto1 = productoService.añadir(
                                        new Producto(0L, "Lechuga", 10, true, "REDUCIDO", 1000d, "Imagenes/lechuga.jpg",
                                                        cat2));
                        Producto producto2 = productoService.añadir(
                                        new Producto(0L, "Manzana", 25, true, "NORMAL", 2000d, "Imagenes/manzanas.jpg",
                                                        cat1));
                        Producto producto3 = productoService.añadir(
                                        new Producto(0L, "Ternera", 45, true, "NORMAL", 4000d, "Imagenes/Ternera.jpg",
                                                        cat3));
                        Producto producto4 = productoService.añadir(
                                        new Producto(0L, "Salmon", 45, true, "NORMAL", 4000d, "Imagenes/salmon.jpg",
                                                        cat4));
                        Producto producto5 = productoService.añadir(
                                        new Producto(0L, "Tinto", 45, true, "NORMAL", 4000d, "Imagenes/tinto.png",
                                                        cat5));
                        Pedido pedido1 = pedidoService.añadir(
                                        new Pedido(0L, LocalDate.of(2003, 3, 12), 3000d, cliente1));
                        Pedido pedido2 = pedidoService.añadir(
                                        new Pedido(0L, LocalDate.of(2024, 6, 17), 5000d, cliente3));

                        detallePedidoService.añadir(new DetallePedido(
                                        0L, pedido1, producto1, 3));
                        detallePedidoService.añadir(new DetallePedido(
                                        0L, pedido2, producto1, 1));
                        detallePedidoService.añadir(new DetallePedido(
                                        0L, pedido2, producto2, 2));
                        usuarioService.añadir(
                                        new Usuario(0L, "Paco", LocalDate.of(2003, 3, 12),
                                                        "1234", Rol.ADMIN));
                        usuarioService.añadir(
                                        new Usuario(0L, "Julia", LocalDate.of(2024, 6, 17),
                                                        "0000", Rol.MANAGER));
                        usuarioService.añadir(
                                        new Usuario(0L, "Raul", LocalDate.of(2023, 8, 21),
                                                        "5678", Rol.USER));
                };
        }
}
