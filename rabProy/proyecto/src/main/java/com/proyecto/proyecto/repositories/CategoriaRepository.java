package com.proyecto.proyecto.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyecto.proyecto.domain.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
