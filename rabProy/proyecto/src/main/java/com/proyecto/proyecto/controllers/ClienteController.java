package com.proyecto.proyecto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.proyecto.proyecto.domain.Cliente;
import com.proyecto.proyecto.services.ClienteService;

import jakarta.validation.Valid;

@Controller
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    public ClienteService clienteService;

    @GetMapping({ "/", "/list" })
    public String showList(Model model) {
        model.addAttribute("listaClientes", clienteService.obtenerTodos());
        return "cliente/listView";
    }

    @GetMapping("/new")
    public String showNew(Model model) {
        model.addAttribute("clienteForm", new Cliente());
        return "cliente/newFormView";
    }

    @PostMapping("/new/submit")
    public String showNewSubmit(@Valid Cliente clienteForm,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return "redirect:/clientes/new";
        clienteService.añadir(clienteForm);
        return "redirect:/clientes/list";
    }

    @GetMapping("/delete/{id}")
    public String showDelete(@PathVariable Long id) {
        clienteService.borrar(clienteService.obtenerPorId(id));
        return "redirect:/clientes/list";
    }

    @GetMapping("/editar/{id}")
    public String showEditForm(@PathVariable Long id, Model model) {
        Cliente cliente = clienteService.obtenerPorId(id);
        if (cliente != null) {
            model.addAttribute("clienteForm", cliente);
            return "cliente/editFormView";
        } else {
            return "redirect:/clientes/list";
        }
    }

    @PostMapping("/editar/submit")
    public String showEditSubmit(@Valid Cliente clienteForm,
            BindingResult bindingResult) {
        if (!bindingResult.hasErrors())
            clienteService.editar(clienteForm);
        return "redirect:/clientes/list";
    }
}
