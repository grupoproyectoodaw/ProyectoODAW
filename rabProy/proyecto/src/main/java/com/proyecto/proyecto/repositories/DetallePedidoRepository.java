package com.proyecto.proyecto.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyecto.proyecto.domain.DetallePedido;
import com.proyecto.proyecto.domain.Pedido;
import com.proyecto.proyecto.domain.Producto;

@Repository
public interface DetallePedidoRepository extends JpaRepository<DetallePedido, Long> {
    List<DetallePedido> findByPedido(Pedido pedido);

    List<DetallePedido> findByProducto(Producto producto);

    DetallePedido findByPedidoAndProducto(Pedido pedido, Producto producto);
}
