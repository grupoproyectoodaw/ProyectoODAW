package com.proyecto.proyecto.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Carrito {
    private List<ItemCarrito> items = new ArrayList<>();

    public void agregarProducto(Producto producto, int cantidad) {
        for (ItemCarrito item : items) {
            if (item.getProducto().getId().equals(producto.getId())) {
                item.setCantidad(item.getCantidad() + cantidad);
                return;
            }
        }
        items.add(new ItemCarrito(producto, cantidad));
    }

    public boolean actualizarCantidadProducto(Long id, int cantidad) {
        for (ItemCarrito item : items) {
            if (item.getProducto().getId().equals(id)) {
                item.setCantidad(cantidad);
                return true;
            }
        }
        return false;
    }

    public List<ItemCarrito> getItems() {
        return items;
    }

    public void eliminarProducto(int index) {
        if (index >= 0 && index < items.size()) {
            items.remove(index);
        }
    }
}
