package com.proyecto.proyecto.domain;

public enum Rol {
    USER, MANAGER, ADMIN
}
