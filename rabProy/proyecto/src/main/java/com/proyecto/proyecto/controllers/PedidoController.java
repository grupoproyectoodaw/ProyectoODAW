package com.proyecto.proyecto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.proyecto.proyecto.domain.Pedido;
import com.proyecto.proyecto.services.ClienteService;
import com.proyecto.proyecto.services.PedidoService;

@Controller
@RequestMapping("/pedidos")
public class PedidoController {
    @Autowired
    PedidoService pedidoService;

    @Autowired
    ClienteService clienteService;

    @GetMapping("/")
    public String showAll(Model model) {
        model.addAttribute("listaPedidos", pedidoService.obtenerTodos());
        return "pedido/listView";
    }

    @GetMapping("/nuevo")
    public String showNew(Model model) {
        model.addAttribute("pedidoForm", new Pedido());
        model.addAttribute("listaClientes", clienteService.obtenerTodos());
        return "pedido/newFormView";
    }

    @PostMapping("/nuevo/submit")
    public String showNewSubmit(Pedido pedidoForm, Model model) {
        pedidoService.añadir(pedidoForm);
        return "redirect:/pedidos/";
    }

    @GetMapping("/editar/{id}")
    public String showEdit(@PathVariable Long id, Model model) {
        Pedido pedidoForm;
        pedidoForm = pedidoService.obtenerPorId(id);
        model.addAttribute("pedidoForm", pedidoForm);
        model.addAttribute("listaClientes", clienteService.obtenerTodos());
        return "pedido/editFormView";
    }

    @PostMapping("/editar/submit")
    public String showEditSubmit(Pedido pedidoForm, Model model) {
        pedidoService.editar(pedidoForm);
        return "redirect:/pedidos/";
    }

    @GetMapping("/borrar/{id}")
    public String showDelete(@PathVariable Long id, Model model) {
        pedidoService.borrar(id);
        return "redirect:/pedidos/";
    }
}
