package com.proyecto.proyecto.excepciones;

public class UsuarioDuplicadoException extends RuntimeException {
    public UsuarioDuplicadoException(String mensaje) {
        super(mensaje);
    }

    public UsuarioDuplicadoException() {
        super("usuario duplicado");
    }
}
