package com.proyecto.proyecto.services;

import java.util.List;

import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.domain.Usuario;
import com.proyecto.proyecto.domain.Valoracion;

public interface ValoracionService {

    Valoracion añadir(Valoracion valoracion);

    List<Valoracion> mostrarListaValoracion();

    Valoracion obtenerPorId(Long id);

    void borrar(Valoracion valoracion);

    List<Valoracion> obtenerPorUsuario(Usuario usuario);

    List<Valoracion> obtenerPorProducto(Producto producto);

    Valoracion obtenerPorUsuarioProducto(Usuario u, Producto p);

    public Usuario obtenerUsuarioConectado();
}
