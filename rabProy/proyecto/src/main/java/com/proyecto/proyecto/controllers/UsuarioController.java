package com.proyecto.proyecto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.proyecto.proyecto.domain.Usuario;
import com.proyecto.proyecto.services.UsuarioService;

@Controller
@RequestMapping("/usuarios")
public class UsuarioController {
    @Autowired
    UsuarioService usuarioService;

    @GetMapping({ "", "/" })
    public String showUsuarios(Model model) {
        model.addAttribute("listaUsuarios", usuarioService.mostrarListaUsuarios());
        return "usuarios/usuariosView";
    }

    @GetMapping("/nuevo")
    public String showNew(Model model) {
        model.addAttribute("usuarioForm", new Usuario());
        return "usuarios/newFormView";
    }

    @PostMapping("/nuevo/submit")
    public String showNewSubmit(Usuario usuarioForm, Model model) {
        usuarioService.añadir(usuarioForm);
        return "redirect:/usuarios/";
    }

    @GetMapping("/editar/{id}")
    public String showEdit(@PathVariable Long id, Model model) {
        Usuario usuarioForm;
        usuarioForm = usuarioService.obtenerPorId(id);
        model.addAttribute("usuarioForm", usuarioForm);
        return "usuarios/editFormView";
    }

    @PostMapping("/editar/submit")
    public String showEditSubmit(Usuario usuarioForm, Model model) {
        usuarioService.editar(usuarioForm);
        return "redirect:/usuarios/";
    }

    @GetMapping("/borrar/{id}")
    public String showDelete(@PathVariable Long id, Model model) {
        usuarioService.borrar(id);
        return "redirect:/usuarios/";
    }
}
