package com.proyecto.proyecto.config;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity

public class SecurityConfig {
    @Bean
    AuthenticationManager authenticationManager(
            AuthenticationConfiguration authenticationConfiguration)
            throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.headers(
                headersConfigurer -> headersConfigurer
                        .frameOptions(HeadersConfigurer.FrameOptionsConfig::sameOrigin));
        http.authorizeHttpRequests(auth -> auth
                .requestMatchers("/").permitAll()
                .requestMatchers("/productos/").permitAll()
                .requestMatchers("/productos/nuevo/**", "/productos/editar/**", "/productos/borrar/**",
                        "/productos/porCateg/**")
                .hasAnyRole("MANAGER", "ADMIN")
                .requestMatchers("/carrito").hasAnyRole("MANAGER", "ADMIN", "USER")
                .requestMatchers("/carrito/agregar/**", "/carrito/actualizar/**", "/carrito/eliminar/**").hasAnyRole("MANAGER", "ADMIN", "USER")
                .requestMatchers("/categorias/").hasAnyRole("MANAGER", "ADMIN", "USER")
                .requestMatchers("/categorias/nuevo/**", "/categorias/editar/**",
                        "/categorias/borrar/**")
                .hasAnyRole("MANAGER", "ADMIN")
                .requestMatchers("/clientes/").hasAnyRole("MANAGER", "ADMIN", "USER")
                .requestMatchers("/clientes/new/**", "/clientes/delete/**", "/clientes/editar/**")
                .hasAnyRole("MANAGER", "ADMIN")
                .requestMatchers("/detallePedido/pedido/**", "/detallePedido/producto/**", "/detallePedido/delete/**",
                        "/detallePedido/new/**")
                .hasAnyRole("MANAGER", "ADMIN")
                .requestMatchers("/pedidos/").hasAnyRole("MANAGER", "ADMIN", "USER")
                .requestMatchers("/pedidos/nuevo/**", "/pedidos/editar/**", "/pedidos/borrar/**")
                .hasAnyRole("MANAGER", "ADMIN")
                .requestMatchers("/valoraciones/new/**").hasAnyRole("USER", "MANAGER", "ADMIN")
                .requestMatchers("/valoraciones/usuario/**", "/valoraciones/producto/**",
                        "/valoraciones/delete/**")
                .hasAnyRole("MANAGER", "ADMIN")
                .requestMatchers("/usuarios/", "/usuarios/nuevo/**", "/usuarios/editar/**",
                        "/usuarios/borrar/**")
                .hasAnyRole("ADMIN")
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations())
                .permitAll() // para rutas: /css, /js /images
                .anyRequest().permitAll())
                .formLogin(formLogin -> formLogin
                        .defaultSuccessUrl("/", true)
                        .permitAll())
                .logout(logout -> logout
                        .logoutSuccessUrl("/")
                        .permitAll())
                // .csrf(csrf -> csrf.disable())
                .httpBasic(Customizer.withDefaults())
                .rememberMe(Customizer.withDefaults());
        http.exceptionHandling(exceptions -> exceptions.accessDeniedPage("/"));
        return http.build();
    }
}
