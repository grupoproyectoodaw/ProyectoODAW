package com.proyecto.proyecto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.proyecto.proyecto.domain.DetallePedido;
import com.proyecto.proyecto.domain.Pedido;
import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.services.DetallePedidoService;
import com.proyecto.proyecto.services.PedidoService;
import com.proyecto.proyecto.services.ProductoService;

import jakarta.validation.Valid;

@Controller
@RequestMapping("/detallePedido")
public class DetallePedidoController {
    @Autowired
    public DetallePedidoService detallePedidoService;
    @Autowired
    PedidoService pedidoService;
    @Autowired
    ProductoService productoService;

    @GetMapping("/pedido/{id}")
    public String obtenerPedidoPorProducto(@PathVariable Long id, Model model) {
        Pedido p = pedidoService.obtenerPorId(id);
        model.addAttribute("listaDetalle_Pedidos", detallePedidoService.obtenerPorPedido(p));
        model.addAttribute("pedido", pedidoService.obtenerPorId(id));
        return "detallePedido/pedidoListView";
    }

    @GetMapping("/producto/{id}")
    public String obtenerProductoPorPedido(@PathVariable Long id, Model model) {
        Producto pr = productoService.obtenerPorId(id);
        model.addAttribute("listadetalle_Pedidos", detallePedidoService.obtenerPorProducto(pr));
        model.addAttribute("producto", productoService.obtenerPorId(id));
        return "detallePedido/productoListView";
    }

    @GetMapping("/delete/{id}")
    public String showDeletePedido(@PathVariable Long id) {
        detallePedidoService.borrar(detallePedidoService.obtenerPorId(id));
        return "redirect:/pedidos/";
    }

    @GetMapping("/new")
    public String showNewDetalle_Pedido(Model model) {
        model.addAttribute("detalle_PedidoForm", new DetallePedido());
        model.addAttribute("listaPedidos", pedidoService.obtenerTodos());
        model.addAttribute("listaProductos", productoService.obtenerTodos());
        return "detallePedido/newFormView";
    }

    @PostMapping("/new/submit")
    public String showNewProjectEmplSubmit(@Valid DetallePedido detallePedidoForm, BindingResult bindingResult) {
        if (!bindingResult.hasErrors())
            detallePedidoService.añadir(detallePedidoForm);
        return "redirect:/pedidos/";
    }
}
