package com.proyecto.proyecto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.proyecto.proyecto.domain.Categoria;
import com.proyecto.proyecto.services.CategoriaService;

@Controller
@RequestMapping("/categorias")
public class CategoriaController {

    @Autowired
    public CategoriaService categoriaService;

    @GetMapping({ "", "/" })
    public String showCategorias(Model model) {
        model.addAttribute("listaCat", categoriaService.mostrarListaCategorias());
        return "categorias/categoriasView";
    }

    @GetMapping("/nuevo")
    public String showNew(Model model) {
        model.addAttribute("categoriaForm", new Categoria());
        return "categorias/newFormView";
    }

    @PostMapping("/nuevo/submit")
    public String showNewSubmit(Categoria categoriaForm, Model model) {
        categoriaService.añadir(categoriaForm);
        return "redirect:/categorias/";
    }

    @GetMapping("/editar/{id}")
    public String showEdit(@PathVariable Long id, Model model) {
        Categoria categoriaForm;
        categoriaForm = categoriaService.obtenerPorId(id);
        model.addAttribute("categoriaForm", categoriaForm);
        return "categorias/editFormView";
    }

    @PostMapping("/editar/submit")
    public String showEditSubmit(Categoria categoriaForm, Model model) {
        categoriaService.editar(categoriaForm);
        return "redirect:/categorias/";
    }

    @GetMapping("/borrar/{id}")
    public String showDelete(@PathVariable Long id, Model model) {
        categoriaService.borrar(id);
        return "redirect:/categorias/";
    }
}
