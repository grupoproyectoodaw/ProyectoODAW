package com.proyecto.proyecto.services;

import java.util.List;

import com.proyecto.proyecto.domain.DetallePedido;
import com.proyecto.proyecto.domain.Pedido;
import com.proyecto.proyecto.domain.Producto;

public interface DetallePedidoService {
    DetallePedido obtenerPorId(Long id);

    DetallePedido añadir(DetallePedido detallePedido);

    void borrar(DetallePedido detallePedido);

    List<DetallePedido> obtenerPorPedido(Pedido pedido);

    List<DetallePedido> obtenerPorProducto(Producto producto);

    DetallePedido obtenerPorDetallePedido(Pedido p, Producto pr);
}
