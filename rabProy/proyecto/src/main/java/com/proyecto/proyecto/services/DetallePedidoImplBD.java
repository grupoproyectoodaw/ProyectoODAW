package com.proyecto.proyecto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.proyecto.domain.DetallePedido;
import com.proyecto.proyecto.domain.Pedido;
import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.repositories.DetallePedidoRepository;

@Service
public class DetallePedidoImplBD implements DetallePedidoService {
    @Autowired
    DetallePedidoRepository detallePedidoRepository;

    public DetallePedido obtenerPorId(Long id) {
        return detallePedidoRepository.findById(id).orElse(null);
    }

    public DetallePedido añadir(DetallePedido detallePedido) {
        return detallePedidoRepository.save(detallePedido);
    }

    public void borrar(DetallePedido detallePedido) {
        detallePedidoRepository.delete(detallePedido);
    }

    public List<DetallePedido> obtenerPorPedido(Pedido pedido) {
        return detallePedidoRepository.findByPedido(pedido);
    }

    public List<DetallePedido> obtenerPorProducto(Producto producto) {
        return detallePedidoRepository.findByProducto(producto);
    }

    public DetallePedido obtenerPorDetallePedido(Pedido p, Producto pr) {
        return detallePedidoRepository.findByPedidoAndProducto(p, pr);
    }
}
