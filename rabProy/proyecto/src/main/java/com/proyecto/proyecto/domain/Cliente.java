package com.proyecto.proyecto.domain;

import org.hibernate.validator.constraints.Length;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")

@Entity
public class Cliente {
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String nombre;

    private String apellido;

    @Length(max = 9)
    private String telefono;

    @Email
    private String email;

    private String direccion;

}
