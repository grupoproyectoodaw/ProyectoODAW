package com.proyecto.proyecto.domain;

import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FormInfo {
    private String nombre;
    private String email;
    public String lista;
    public String comentario;
    @NotNull
    @AssertTrue
    boolean check;
}
