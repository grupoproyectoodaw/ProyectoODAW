package com.proyecto.proyecto.services;

import java.util.List;

import com.proyecto.proyecto.domain.Categoria;

public interface CategoriaService {

    Categoria añadir(Categoria categoria);

    List<Categoria> mostrarListaCategorias();

    Categoria obtenerPorId(Long id);

    Categoria editar(Categoria categoria);

    void borrar(Long id);
}
