package com.proyecto.proyecto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.proyecto.domain.Pedido;
import com.proyecto.proyecto.repositories.ClienteRepository;
import com.proyecto.proyecto.repositories.PedidoRepository;

@Service
public class PedidoServiceImplBD implements PedidoService{
    @Autowired
    PedidoRepository pedidoRepository;
    @Autowired
    ClienteRepository clienteRepository;

    public Pedido añadir (Pedido pedido) {
        return pedidoRepository.save (pedido);
    }

    public List<Pedido> obtenerTodos() { 
        return pedidoRepository.findAll (); 
    }

    public Pedido obtenerPorId (Long id) {
        return pedidoRepository.findById (id).orElse(null);
    }

    public Pedido editar (Pedido pedido) {
        return pedidoRepository.save (pedido);
    }

    public void borrar(Long id) {
        pedidoRepository.deleteById (id);
    }

    public List<Pedido> obtenerPorCliente(Long idCliente){
        return pedidoRepository.findByCliente(clienteRepository.findById(idCliente).get());
    }
}
