package com.proyecto.proyecto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.proyecto.proyecto.domain.Producto;
import com.proyecto.proyecto.domain.Usuario;
import com.proyecto.proyecto.domain.Valoracion;
import com.proyecto.proyecto.excepciones.ValoracionException;
import com.proyecto.proyecto.services.ProductoService;
import com.proyecto.proyecto.services.UsuarioService;
import com.proyecto.proyecto.services.ValoracionService;

import jakarta.validation.Valid;

@Controller
@RequestMapping("/valoraciones")
public class ValoracionController {
    @Autowired
    ValoracionService valoracionService;
    @Autowired
    UsuarioService usuarioService;
    @Autowired
    ProductoService productoService;

    @GetMapping("/usuario/{id}")
    public String showUsersByProduct(@PathVariable long id, Model model) {
        Usuario u = usuarioService.obtenerPorId(id);
        model.addAttribute("listaValoraciones",
                valoracionService.obtenerPorUsuario(u));
        model.addAttribute("usuario", usuarioService.obtenerPorId(id));
        return "valoraciones/usuarioListView";
    }

    @GetMapping("/producto/{id}")
    public String showProductbyUsers(@PathVariable long id, Model model) {
        Producto p = productoService.obtenerPorId(id);
        model.addAttribute("listaValoraciones",
                valoracionService.obtenerPorProducto(p));
        model.addAttribute("producto", productoService.obtenerPorId(id));
        return "valoraciones/productoListView";
    }

    @GetMapping("/delete/{id}")
    public String showDeleteUsuario(@PathVariable long id) {
        try {
            valoracionService.borrar(valoracionService.obtenerPorId(id));
        } catch (ValoracionException e) {
            return "redirect:/productos/?error";
        }
        return "redirect:/productos/";
    }

    @GetMapping("/new")
    public String showNewValoracion(Model model) {
        model.addAttribute("valoracionForm", new Valoracion());
        model.addAttribute("listaUsuarios", usuarioService.mostrarListaUsuarios());
        model.addAttribute("listaProductos", productoService.obtenerTodos());
        return "valoraciones/newFormView";
    }

    @PostMapping("/new/submit")
    public String showNewProjectEmplSubmit(@Valid Valoracion valoracionForm,
            BindingResult bindingResult) {
        if (!bindingResult.hasErrors())
            valoracionService.añadir(valoracionForm);
        return "redirect:/";
    }
}
