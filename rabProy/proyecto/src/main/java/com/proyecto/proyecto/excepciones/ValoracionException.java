package com.proyecto.proyecto.excepciones;

public class ValoracionException extends RuntimeException {
    public ValoracionException(String mensaje) {
        super(mensaje);
    }

    public ValoracionException() {
        super("valoracion inexistente");
    }
}
