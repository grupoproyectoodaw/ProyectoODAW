package com.proyecto.proyecto.services;

import java.util.List;

import com.proyecto.proyecto.domain.Producto;

public interface ProductoService {
    Producto añadir(Producto producto);

    List<Producto> obtenerTodos();

    Producto obtenerPorId(Long id);

    Producto editar(Producto producto);

    void borrar(Long id);

    List<Producto> obtenerPorCategoria(Long idCategoria);

    public void actualizarProducto(Producto producto);
}
