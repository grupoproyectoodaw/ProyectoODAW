package com.proyecto.proyecto.services;

import java.util.List;

import com.proyecto.proyecto.domain.Cliente;

public interface ClienteService {
    Cliente añadir(Cliente cliente);
    List<Cliente> obtenerTodos();
    Cliente obtenerPorId(Long id);
    Cliente editar(Cliente cliente);
    void borrar(Cliente Cliente);
}

