package com.proyecto.proyecto.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")

@Entity
public class Producto {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String nombre;

    private Integer cantidad;

    private Boolean enOferta;

    @NotEmpty
    private String tipoIva;

    private Double precio;

    private String imageUrl;

    @ManyToOne
    private Categoria categoria;
}
