package com.proyecto.proyecto.services;

import java.util.List;

import com.proyecto.proyecto.domain.Pedido;

public interface PedidoService {
    Pedido añadir(Pedido pedido);
    List<Pedido> obtenerTodos();
    Pedido obtenerPorId(Long id);
    Pedido editar(Pedido pedido);
    void borrar(Long id);

    List<Pedido> obtenerPorCliente(Long idCliente);
}
