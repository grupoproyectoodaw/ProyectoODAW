package com.proyecto.proyecto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.proyecto.domain.Cliente;
import com.proyecto.proyecto.repositories.ClienteRepository;

@Service
public class ClienteServiceImplBD implements ClienteService{
    @Autowired
    ClienteRepository clienteRepository;

    public Cliente añadir(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public List<Cliente> obtenerTodos(){
        return clienteRepository.findAll();
    }

    public Cliente obtenerPorId(Long id){
        return clienteRepository.findById(id).orElse(null);
    }

    public Cliente editar(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public void borrar(Cliente cliente) {
        clienteRepository.delete(cliente);
    }

}
