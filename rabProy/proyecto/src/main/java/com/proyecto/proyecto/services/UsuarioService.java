package com.proyecto.proyecto.services;

import java.util.List;

import com.proyecto.proyecto.domain.Usuario;

public interface UsuarioService {
    Usuario añadir(Usuario usuario);

    List<Usuario> mostrarListaUsuarios();

    Usuario obtenerPorId(Long id);

    Usuario editar(Usuario usuario);

    void borrar(Long id);

    Usuario obtenerUsuarioConectado();
}
