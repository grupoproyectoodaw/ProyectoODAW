document.addEventListener("DOMContentLoaded", function () {
    var form = document.getElementById("productoForm");

    form.addEventListener("submit", function (event) {
        event.preventDefault();
        console.log("Evento submit detectado");

        var nombreInput = document.getElementById("nombre");
        var cantidadInput = document.getElementById("cantidad");
        var precioInput = document.getElementById("precio");
        var categoriaSelect = document.getElementById("categoria");

        var errorShown = false;

        if (nombreInput.value.trim() === "") {
            nombreInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un nombre");
                errorShown = true;
            }
        } else {
            nombreInput.classList.remove("error");
        }

        var cantidadValue = cantidadInput.value.trim();
        var cantidadRegex = /^\d+$/;
        if (cantidadValue === "") {
            cantidadInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir una cantidad");
                errorShown = true;
            }
        } else if (!cantidadRegex.test(cantidadValue)) {
            cantidadInput.classList.add("error");
            if (!errorShown) {
                alert("La cantidad debe ser un número entero");
                errorShown = true;
            }
        } else {
            cantidadInput.classList.remove("error");
        }

        var precioValue = precioInput.value.trim();
        var precioRegex = /^\d+(\.\d+)?d?$/;
        if (precioValue === "") {
            precioInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un precio");
                errorShown = true;
            }
        } else if (!precioRegex.test(precioValue)) {
            precioInput.classList.add("error");
            if (!errorShown) {
                alert("El precio debe ser un número flotante, con al menos un decimal o la letra 'd'");
                errorShown = true;
            }
        } else {
            precioInput.classList.remove("error");
        }

        if (categoriaSelect.value === "") {
            categoriaSelect.classList.add("error");
            if (!errorShown) {
                alert("Debes seleccionar una categoría");
                errorShown = true;
            }
        } else {
            categoriaSelect.classList.remove("error");
        }

        if (!errorShown) {
            form.submit();
        }
    });
});