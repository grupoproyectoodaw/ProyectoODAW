document.addEventListener("DOMContentLoaded", function () {
    var form = document.getElementById("categoriaForm");

    form.addEventListener("submit", function (event) {
        event.preventDefault();
        console.log("Evento submit detectado");

        var nombreInput = document.getElementById("nombre");

        var errorShown = false;

        if (nombreInput.value.trim() === "") {
            nombreInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un nombre");
                errorShown = true;
            }
        } else if (nombreInput.value.trim().length > 25) {
            nombreInput.classList.add("error");
            nombreInput.value = "";
            if (!errorShown) {
                alert("El nombre no puede pasar de 25 letras");
                errorShown = true;
            }
        } else {
            nombreInput.classList.remove("error");
        }

        if (!errorShown) {
            form.submit();
        }
    });
});
