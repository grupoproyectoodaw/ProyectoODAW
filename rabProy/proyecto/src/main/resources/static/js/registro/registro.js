document.addEventListener("DOMContentLoaded", function () {
    var form = document.getElementById("registroForm");

    form.addEventListener("submit", function (event) {
        event.preventDefault();
        console.log("Evento submit detectado");

        var nombreInput = document.getElementById("nombre");
        var contraseñaInput = document.getElementById("contraseña");
        var fechaNacimientoInput = document.getElementById("fechaNacimiento");

        var errorShown = false;

        if (nombreInput.value.trim() === "") {
            nombreInput.classList.add("error");
            if (!errorShown) {
                alert("El nombre no puede estar vacío");
                errorShown = true;
            }
        } else {
            nombreInput.classList.remove("error");
        }

        if (contraseñaInput.value.trim() === "") {
            contraseñaInput.classList.add("error");
            if (!errorShown) {
                alert("La contraseña no puede estar vacía");
                errorShown = true;
            }
        } else {
            contraseñaInput.classList.remove("error");
        }

        if (fechaNacimientoInput.value === "") {
            fechaNacimientoInput.classList.add("error");
            if (!errorShown) {
                alert("La fecha de nacimiento no puede estar vacía");
                errorShown = true;
            }
        } else {
            fechaNacimientoInput.classList.remove("error");
        }

        if (!errorShown) {
            form.submit();
        }
    });
});