document.addEventListener("DOMContentLoaded", function () {
    var form = document.getElementById("editarUsuarioForm");

    form.addEventListener("submit", function (event) {
        event.preventDefault();
        console.log("Evento submit detectado");

        var nombreInput = document.getElementById("nombre");
        var fechaInput = document.getElementById("fecha");
        var contraseñaInput = document.getElementById("contraseña");
        var rolInput = document.getElementById("rol");

        var errorShown = false;

        if (nombreInput.value.trim() === "") {
            nombreInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un nombre");
                errorShown = true;
            }
        } else {
            nombreInput.classList.remove("error");
        }

        if (fechaInput.value.trim() === "") {
            fechaInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir una fecha");
                errorShown = true;
            }
        } else {
            fechaInput.classList.remove("error");
        }

        if (contraseñaInput.value.trim() === "") {
            contraseñaInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir una contraseña");
                errorShown = true;
            }
        } else {
            contraseñaInput.classList.remove("error");
        }

        var rolValue = rolInput.value.trim().toUpperCase();
        var validRoles = ["USER", "MANAGER", "ADMIN"];
        if (!validRoles.includes(rolValue)) {
            rolInput.classList.add("error");
            if (!errorShown) {
                alert("El rol debe ser uno de los siguientes valores: USER, MANAGER, ADMIN");
                errorShown = true;
            }
        } else {
            rolInput.classList.remove("error");
        }

        if (!errorShown) {
            console.log("Formulario válido, enviando...");
            form.submit();
        }
    });
});