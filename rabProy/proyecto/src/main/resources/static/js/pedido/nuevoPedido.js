document.addEventListener("DOMContentLoaded", function () {
    var form = document.getElementById("pedidoForm");

    form.addEventListener("submit", function (event) {
        event.preventDefault();
        console.log("Evento submit detectado");

        var fechaInput = document.getElementById("fecha");
        var precioInput = document.getElementById("precio");
        var clienteSelect = document.getElementById("cliente");

        var errorShown = false;

        if (fechaInput.value.trim() === "") {
            fechaInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir una fecha");
                errorShown = true;
            }
        } else {
            fechaInput.classList.remove("error");
        }

        var precioValue = precioInput.value.trim();
        var precioRegex = /^\d+(\.\d+)?d?$/;
        if (precioValue === "") {
            precioInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un precio");
                errorShown = true;
            }
        } else if (!precioRegex.test(precioValue)) {
            precioInput.classList.add("error");
            if (!errorShown) {
                alert("El precio debe ser un número flotante, con al menos un decimal o la letra 'd'");
                errorShown = true;
            }
        } else {
            precioInput.classList.remove("error");
        }

        if (clienteSelect.value === "0") {
            clienteSelect.classList.add("error");
            if (!errorShown) {
                alert("Debes seleccionar un cliente");
                errorShown = true;
            }
        } else {
            clienteSelect.classList.remove("error");
        }

        if (!errorShown) {
            form.submit();
        }
    });
});