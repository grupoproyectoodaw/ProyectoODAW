document.addEventListener("DOMContentLoaded", function () {
    var form = document.getElementById("clienteForm");

    form.addEventListener("submit", function (event) {
        event.preventDefault();
        console.log("Evento submit detectado");

        var nombreInput = document.getElementById("nombre");
        var apellidoInput = document.getElementById("apellido");
        var telefonoInput = document.getElementById("telefono");
        var emailInput = document.getElementById("email");
        var direccionInput = document.getElementById("direccion");

        var errorShown = false;

        if (nombreInput.value.trim() === "") {
            nombreInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un nombre");
                errorShown = true;
            }
        } else if (nombreInput.value.trim().length > 25) {
            nombreInput.classList.add("error");
            nombreInput.value = "";
            if (!errorShown) {
                alert("El nombre no puede pasar de 25 letras");
                errorShown = true;
            }
        } else {
            nombreInput.classList.remove("error");
        }

        if (apellidoInput.value.trim() === "") {
            apellidoInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un apellido");
                errorShown = true;
            }
        } else {
            apellidoInput.classList.remove("error");
        }

        var telefonoRegex = /^\d{9}$/;
        if (telefonoInput.value.trim() === "") {
            telefonoInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un teléfono");
                errorShown = true;
            }
        } else if (!telefonoRegex.test(telefonoInput.value.trim())) {
            telefonoInput.classList.add("error");
            telefonoInput.value = "";
            if (!errorShown) {
                alert("El teléfono debe tener 9 números");
                errorShown = true;
            }
        } else {
            telefonoInput.classList.remove("error");
        }

        var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (emailInput.value.trim() === "") {
            emailInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un email");
                errorShown = true;
            }
        } else if (!emailRegex.test(emailInput.value.trim())) {
            emailInput.classList.add("error");
            emailInput.value = "";
            if (!errorShown) {
                alert("Debes introducir un email válido");
                errorShown = true;
            }
        } else {
            emailInput.classList.remove("error");
        }

        if (direccionInput.value.trim() === "") {
            direccionInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir una dirección");
                errorShown = true;
            }
        } else {
            direccionInput.classList.remove("error");
        }

        if (!errorShown) {
            form.submit();
        }
    });
});