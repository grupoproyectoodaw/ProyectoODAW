document.addEventListener("DOMContentLoaded", function () {
    var form = document.getElementById("valoracionForm");

    form.addEventListener("submit", function (event) {
        event.preventDefault();
        console.log("Evento submit detectado");

        var usuarioSelect = document.getElementById("usuario");
        var productoSelect = document.getElementById("producto");
        var puntuacionInput = document.getElementById("puntuacion");
        var comentarioInput = document.getElementById("comentario");

        var errorShown = false;

        if (usuarioSelect.value === "0" && productoSelect.value === "0") {
            usuarioSelect.classList.add("error");
            productoSelect.classList.add("error");
            if (!errorShown) {
                alert("Debes seleccionar al menos un usuario o un producto");
                errorShown = true;
            }
        } else {
            usuarioSelect.classList.remove("error");
            productoSelect.classList.remove("error");
        }

        var puntuacionValue = parseFloat(puntuacionInput.value.trim());
        if (isNaN(puntuacionValue) || puntuacionValue <= 0) {
            puntuacionInput.classList.add("error");
            if (!errorShown) {
                alert("La puntuación debe ser un número positivo");
                errorShown = true;
            }
        } else {
            puntuacionInput.classList.remove("error");
        }

        if (comentarioInput.value.trim() === "") {
            comentarioInput.classList.add("error");
            if (!errorShown) {
                alert("El comentario no puede estar vacío");
                errorShown = true;
            }
        } else {
            comentarioInput.classList.remove("error");
        }

        if (!errorShown) {
            form.submit();
        }
    });
});
