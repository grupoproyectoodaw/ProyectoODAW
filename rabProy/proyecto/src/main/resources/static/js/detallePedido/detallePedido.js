document.addEventListener("DOMContentLoaded", function () {
    console.log("DOM completamente cargado y analizado");

    var form = document.getElementById("detallePedidoForm");

    form.addEventListener("submit", function (event) {
        event.preventDefault();
        console.log("Evento submit detectado");

        var pedidoSelect = document.getElementById("pedido");
        var productoSelect = document.getElementById("producto");
        var cantidadInput = document.getElementById("cantidad");

        var errorShown = false;

        if (pedidoSelect.value === "0") {
            pedidoSelect.classList.add("error");
            console.log("Error: Pedido no seleccionado");
            if (!errorShown) {
                alert("Debes seleccionar un pedido");
                errorShown = true;
            }
        } else {
            pedidoSelect.classList.remove("error");
        }

        if (productoSelect.value === "0") {
            productoSelect.classList.add("error");
            console.log("Error: Producto no seleccionado");
            if (!errorShown) {
                alert("Debes seleccionar un producto");
                errorShown = true;
            }
        } else {
            productoSelect.classList.remove("error");
        }

        var cantidadValue = cantidadInput.value.trim();
        var cantidadRegex = /^\d+$/;
        if (cantidadValue === "") {
            cantidadInput.classList.add("error");
            console.log("Error: Cantidad no introducida");
            if (!errorShown) {
                alert("Debes introducir una cantidad");
                errorShown = true;
            }
        } else if (!cantidadRegex.test(cantidadValue) || parseInt(cantidadValue) <= 0) {
            cantidadInput.classList.add("error");
            console.log("Error: Cantidad inválida");
            if (!errorShown) {
                alert("La cantidad debe ser un número entero positivo");
                errorShown = true;
            }
        } else {
            cantidadInput.classList.remove("error");
        }

        if (!errorShown) {
            console.log("Formulario válido, enviando...");
            form.submit();
        }
    });
});