document.addEventListener("DOMContentLoaded", function() {
    var form = document.getElementById("contactForm");

    form.addEventListener("submit", function(event) {
        event.preventDefault();
        console.log("Evento submit detectado");

        var nombreInput = document.getElementById("nombre");
        var emailInput = document.getElementById("email");
        var listaInput = document.getElementById("lista");
        var comentarioInput = document.getElementById("comentario");
        var checkInput = document.getElementById("check");

        var errorShown = false;

        if (nombreInput.value.trim() === "") {
            nombreInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un nombre");
                errorShown = true;
            }
        } else if (nombreInput.value.trim().length > 25) {
            nombreInput.classList.add("error");
            nombreInput.value = "";
            if (!errorShown) {
                alert("El nombre no puede pasar de 25 letras");
                errorShown = true;
            }
        } else {
            nombreInput.classList.remove("error");
        }

        var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (emailInput.value.trim() === "") {
            emailInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un email");
                errorShown = true;
            }
        } else if (!emailRegex.test(emailInput.value.trim())) {
            emailInput.classList.add("error");
            emailInput.value = "";
            if (!errorShown) {
                alert("Debes introducir un email válido");
                errorShown = true;
            }
        } else {
            emailInput.classList.remove("error");
        }

        if (listaInput.value.trim() === "") {
            listaInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un elemento");
                errorShown = true;
            }
        } else {
            listaInput.classList.remove("error");
        }

        if (comentarioInput.value.trim() === "") {
            comentarioInput.classList.add("error");
            if (!errorShown) {
                alert("Debes introducir un comentario");
                errorShown = true;
            }
        } else {
            comentarioInput.classList.remove("error");
        }

        if (!checkInput.checked) {
            checkInput.classList.add("error");
            if (!errorShown) {
                alert("Debes aceptar las condiciones.");
                errorShown = true;
            }
        } else {
            checkInput.classList.remove("error");
        }

        if (!errorShown) {
            form.submit();
        }
    });
});
